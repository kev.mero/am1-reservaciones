package com.example.reservacionapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListaHoteles extends AppCompatActivity {
    String url = "https://salty-wave-62690.herokuapp.com/api/";

    GridView _grid;
    EditText txtfecha;
    Calendar c;
    DatePickerDialog dpd;
    Button btnbuscar;
    ProgressDialog progress;
    String[] imagenes;


    JSONArray img = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_hoteles);

        progress = new ProgressDialog(this);
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (signInAccount != null) {

        }

        btnbuscar = findViewById(R.id.btnbuscar);
        txtfecha = findViewById(R.id.txtfecha);

        _grid = findViewById(R.id.gridView);

        // fecha
        c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        txtfecha.setText(year + "-" + (month + 1) + "-" + day);
        ObtenerHabitacionesFecha();

        txtfecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);


                dpd = new DatePickerDialog(ListaHoteles.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
                        txtfecha.setText(mYear + "-" + (mMonth + 1) + "-" + mDay);
                    }
                }, year, month, day);
                dpd.show();
            }


        });

        //llamar servicio para obtener las habitaciones
        btnbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ObtenerHabitacionesFecha();
            }
        });


// ir al detalle de la habitacion

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item_salir) {
            FirebaseAuth.getInstance().signOut();
            Intent i = new Intent(ListaHoteles.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void ObtenerHabitacionesFecha() {
        String fecha = txtfecha.getText().toString();
        progress.setMessage("Buscando");
        progress.show();

        //8888888888
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JSONObject postData = new JSONObject();
        try {
            Log.d("fecha nueva", fecha);

            if (fecha.length() == 9) {
                fecha = fecha.substring(0, 8) + "0" + fecha.substring(8, fecha.length());
                Log.d("fecha nueva 333", fecha);
            }
            postData.put("fecha_entrada", fecha);
            //


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //8888888888

        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(
                Request.Method.POST,
                url + "rooms-fechas",
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //convertir
                        //     try {
                        JSONArray data = null;

                        try {
                            data = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        int size = data.length();
                        String[] idList = new String[data.length()];
                        String[] titulos = new String[data.length()];
                        String[] imagenes = new String[data.length()];
                        String[] camas = new String[data.length()];
                        String[] banios = new String[data.length()];
                        String[] precio = new String[data.length()];
                        String[] detalle = new String[data.length()];

                        for (int i = 0; i < size; i++) {
                            try {

                                JSONObject jsonObject = new JSONObject(data.get(i).toString());
                                idList[i] = jsonObject.getString("_id");
                                titulos[i] = jsonObject.getString("nombre");
                                img = jsonObject.getJSONArray("imagen");

                                camas[i] = jsonObject.getString("camas");
                                banios[i] = jsonObject.getString("banios");
                                precio[i] = jsonObject.getString("precio");
                                detalle[i] = jsonObject.getString("detalle");
                                imagenes[i] = img.get(0).toString();


                                // prueba
                                _grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        String[] imgs = new String[img.length()];
                                        String[] listaImagenes = ObtenerHabitacionPorId(idList[position]);

                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        GridAdapter adapter = new GridAdapter(ListaHoteles.this, titulos, imagenes, camas, banios, precio, detalle);
                        _grid.setAdapter(adapter);
                        progress.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(ListaHoteles.this).add(jsonArrayRequest);

    }

    public String[] ObtenerHabitacionPorId(String id) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url + "rooms/" + id,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        //convertir
                        try {
                            JSONObject data = null;
                            data = response.getJSONObject("data");

                            JSONArray imgs = data.getJSONArray("imagen");
                            imagenes = new String[imgs.length()];

                            for (int i = 0; i < imgs.length(); i++) {
                                imagenes[i] = imgs.get(i).toString();

                            }
                            Intent i = new Intent(ListaHoteles.this, DetalleActivity.class);
                            i.putExtra("_id", id);
                            i.putExtra("imagenes", imagenes);
                            i.putExtra("fechaSeleccionada", txtfecha.getText().toString());
                            Log.d("fechaSeleccionada", txtfecha.getText().toString());
                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );

        Volley.newRequestQueue(ListaHoteles.this).add(jsonObjectRequest);
        return imagenes;
    }


}
