package com.example.reservacionapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnIngresar, btnregistrar, btnGoogle;
    EditText txtcorreo,txtcontrasena;
    String correo, contrasena;

    FirebaseAuth mAuth;

    ProgressDialog progress;
    private GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;

    @Override
    protected void onStart(){
        super.onStart();

        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            Intent listaHabitaciones = new Intent(LoginActivity.this, ListaHoteles.class);
            startActivity(listaHabitaciones);
            finish();
        }
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id2))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        btnIngresar = findViewById(R.id.btniniciar_sesion);
        btnregistrar = findViewById(R.id.btn_ir_crearCuenta);
        btnGoogle = findViewById(R.id.btn_google);

        txtcorreo = findViewById(R.id.et_correo);
        txtcontrasena = findViewById(R.id.et_contrasena);


        btnIngresar.setOnClickListener(this);
        btnregistrar.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);

        progress = new ProgressDialog(this);

    }
    @Override
    public void onClick(View view) {
        //Cuando se presione el botón, realiza una acción aquí

        switch (view.getId()){
            case R.id.btniniciar_sesion:
                iniciarSesion();
                break;
            case R.id.btn_ir_crearCuenta:
                Intent registrar = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(registrar);
                finish();
                break;
            case R.id.btn_google:
                signInGoogle();
                break;


        }


    }
    private void iniciarSesion(){
        correo = txtcorreo.getText().toString();
        contrasena = txtcontrasena.getText().toString();
        if (!correo.isEmpty() && !contrasena.isEmpty()) {


            progress.setMessage("Iniciando");
            progress.show();
            mAuth.signInWithEmailAndPassword(correo, contrasena).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if (task.isSuccessful()) {
                        Intent listaHabitaciones = new Intent(LoginActivity.this, ListaHoteles.class);
                        startActivity(listaHabitaciones);
                        finish();
                        Toast.makeText(LoginActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(LoginActivity.this, "Credenciales incorrectas", Toast.LENGTH_LONG).show();

                    }
                    progress.dismiss();

                }

            });
        }else{
            Toast.makeText(LoginActivity.this, "Credenciales incorrectas", Toast.LENGTH_LONG).show();

        }



    }


    private void signInGoogle() {
        progress.setMessage("Iniciando");
        progress.show();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d("TAG 1", "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("TAFG2", "Google sign in failed", e);

            }
            progress.dismiss();

        }

    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG3", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent listaHabitaciones = new Intent(LoginActivity.this, ListaHoteles.class);
                            startActivity(listaHabitaciones);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG4", "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Credenciales incorrectas", Toast.LENGTH_LONG).show();

                        }
                    }
                });
    }



}