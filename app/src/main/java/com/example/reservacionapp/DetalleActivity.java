package com.example.reservacionapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class DetalleActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnVerMas, btnreservar;
    TextView txttitulo,txt_detalle, txtcamas, txtbanios, txtprecio;
    ViewFlipper viewFlipper;
    String url = "https://salty-wave-62690.herokuapp.com/api/";
    String id;
    String[] imagenes;

    String nombreHabitacion, fechaSeleccionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);


        txttitulo = findViewById(R.id.txt_titulo);
        txt_detalle = findViewById(R.id.txt_detalle);
        txtcamas = findViewById(R.id.txtcamas);
        txtbanios = findViewById(R.id.txtbanios);
        txtprecio = findViewById(R.id.txtprecio);
        viewFlipper = findViewById(R.id.grid_image);

        btnVerMas = findViewById(R.id.btn_mas_habitaciones);
        btnreservar = findViewById(R.id.btnreservar);

        btnVerMas.setOnClickListener(this);
        btnreservar.setOnClickListener(this);


        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("_id");
        imagenes = bundle.getStringArray("imagenes");
        fechaSeleccionada = bundle.getString("fechaSeleccionada");
        nombreHabitacion = bundle.getString("_id");

          ObtenerHabitaciones(id);

        for (String img: imagenes) {
            flitterImages(img);
        }
    }

    @Override
    public void onClick(View view) {
        //Cuando se presione el botón, realiza una acción aquí

        switch (view.getId()){
            case R.id.btn_mas_habitaciones:
                Intent habitaciones = new Intent(DetalleActivity.this, ListaHoteles.class);
                startActivity(habitaciones);
                finish();
                break;
            case R.id.btnreservar:
                Intent generar = new Intent(DetalleActivity.this, GenerarQR.class);
                generar.putExtra("fechaSeleccionada", fechaSeleccionada);
                generar.putExtra("nombreHabitacion", nombreHabitacion);
                generar.putExtra("nombre", txttitulo.getText().toString());

                startActivity(generar);

        }
    }


    public void flitterImages(String imageURL){

        ImageView imageView = new ImageView(this);
        Picasso.get().load(imageURL).placeholder(R.drawable.descarga).into(imageView);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(DetalleActivity.this, android.R.anim.slide_in_left);
        viewFlipper.setInAnimation(DetalleActivity.this, android.R.anim.slide_out_right);


    }


    public void ObtenerHabitaciones(String id){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url+"rooms/"+id,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        //convertir
                             try {
                        JSONObject data = null;


                            data = response.getJSONObject("data");
                           // JSONObject jsonObject = new JSONObject(data.get(0).toString());
                                  txttitulo.setText(data.getString("nombre"));
                                    txt_detalle.setText(data.getString("detalle"));
                                    txtcamas.setText("Camas: "+data.getString("camas"));
                                    txtbanios.setText("Baños: "+data.getString("banios"));
                                    txtprecio.setText("Precio: $"+data.getString("precio"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );

        Volley.newRequestQueue(DetalleActivity.this).add(jsonObjectRequest);

    }








}