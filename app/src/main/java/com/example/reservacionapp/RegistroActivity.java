package com.example.reservacionapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {
Button btnIniciarSesion, btnRegistrar;
EditText txtcedula, txtnombres, txttelefono, txtcorreo,txtcontrasena;
String cedula, nombres, telefono, correo, contrasena;

FirebaseAuth mAuth;
DatabaseReference mDatabase;

ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnIniciarSesion = findViewById(R.id.btn_iniciar_sesion);
        btnRegistrar = findViewById(R.id.btn_registrar);

        txtcedula = findViewById(R.id.et_cedula);
        txtnombres = findViewById(R.id.et_nombres);
        txttelefono = findViewById(R.id.et_telefono);
        txtcorreo = findViewById(R.id.et_correo);
        txtcontrasena = findViewById(R.id.et_contrasena);


        btnIniciarSesion.setOnClickListener(this);
        btnRegistrar.setOnClickListener(this);

        progress = new ProgressDialog(this);

    }


    @Override
    public void onClick(View view) {
        //Cuando se presione el botón, realiza una acción aquí

        switch (view.getId()){
            case R.id.btn_iniciar_sesion:
                Intent login = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(login);
                finish();
                break;

            case R.id.btn_registrar:
                cedula = txtcedula.getText().toString();
                nombres = txtnombres.getText().toString();
                telefono = txttelefono.getText().toString();
                correo = txtcorreo.getText().toString();
                contrasena = txtcontrasena.getText().toString();

                if (!cedula.isEmpty() &&!nombres.isEmpty() &&!telefono.isEmpty() &&!correo.isEmpty() &&!contrasena.isEmpty() ){
                    if (contrasena.length() < 6){
                        Toast.makeText(RegistroActivity.this, "La contraseña debe ser mayor o igual a 6 caracteres", Toast.LENGTH_LONG).show();

                    }else{
                        registrarUsuario();
                    }

                }else {
                    Toast.makeText(RegistroActivity.this, "Debe completar todos los campos", Toast.LENGTH_LONG).show();
                }


                break;
        }
    }
    private void registrarUsuario(){
        progress.setMessage("Registrando en linea");
        progress.show();
        mAuth.createUserWithEmailAndPassword(correo, contrasena).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {


                        Map<String, Object> map = new HashMap<>();
                        map.put("cedula", cedula);
                        map.put("nombres", nombres);
                        map.put("telefono", telefono);
                        map.put("correo", correo);
                        map.put("contrasena", contrasena);
                        String id = mAuth.getCurrentUser().getUid();

                        mDatabase.child("Usuarios").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task2) {

                                if (task2.isSuccessful()) {

                                    Intent listaHabitaciones = new Intent(RegistroActivity.this, ListaHoteles.class);
                                    startActivity(listaHabitaciones);
                                    finish();
                                } else {

                                        Toast.makeText(RegistroActivity.this, "No se pudieron crear los datos", Toast.LENGTH_LONG).show();


                                }

                            }
                        });
                    }
               else {
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(RegistroActivity.this, "Este usuario ya existe", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(RegistroActivity.this, "No se pudo registrar este usuario", Toast.LENGTH_LONG).show();

                    }
                }

                progress.dismiss();

            }

        });

    }


}