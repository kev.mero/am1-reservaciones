package com.example.reservacionapp;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


    public class GridAdapter extends BaseAdapter {

        Context context;
        String[] nombre;
        String[] image;
        String[] camas;
        String[] banios;
        String[] precio;
        String[] detalle;

        public GridAdapter(Context context, String[] nombre, String[] image, String[] camas, String[] banios, String[] precio, String[] detalle) {
            super();

            this.context = context;
            this.nombre = nombre;
            this.image = image;
            this.camas = camas;
            this.banios = banios;
            this.precio = precio;
            this.detalle = detalle;
        }


        @Override
        public int getCount() {
            return image.length;
        }

        @Override
        public Object getItem(int i) {
            return image[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v;

            LayoutInflater layoutInflater = LayoutInflater.from(context);

            v = layoutInflater.inflate(R.layout.item_hotel, null);

            String nombreActual = nombre[position];
            String imgActual = image[position];

            TextView txtNombre = v.findViewById(R.id.item_name);
            ImageView imgFoto = v.findViewById(R.id.grid_image);

            txtNombre.setText(nombreActual);
            Picasso.get().load(imgActual).placeholder(R.drawable.descarga).resize(500, 500).centerCrop().into(imgFoto);

            return v;

        }
    }