package com.example.reservacionapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class GenerarQR extends AppCompatActivity {
    DatabaseReference mDatabase;
    EditText txtcedula, txtnombre, txtemail, txttelefono;
    String url = "https://salty-wave-62690.herokuapp.com/api/";
    String fechaSeleccionada, nombreHabitacion, nombre;
    Button btnGenerar,btnatras;
    ProgressDialog progress;
    ImageView viewQr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generar_qr);

        progress = new ProgressDialog(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        viewQr = findViewById(R.id.viewQr);
        txtcedula = findViewById(R.id.txtcedula);
        txtnombre = findViewById(R.id.txtnombre);
        txtemail = findViewById(R.id.txtEmail);
        txttelefono = findViewById(R.id.txtTelefono);
        btnGenerar = findViewById(R.id.btngenerar);
        btnatras = findViewById(R.id.btn_atras);


        obtenerDatosUsuario(user);

        Bundle bundle = getIntent().getExtras();
      fechaSeleccionada = bundle.getString("fechaSeleccionada");
      nombreHabitacion = bundle.getString("nombreHabitacion");
      nombre = bundle.getString("nombre");


      btnatras.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              Intent habitaciones = new Intent(GenerarQR.this, ListaHoteles.class);
              startActivity(habitaciones);
              finish();
          }
      });



      btnGenerar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if (txtcedula.getText().toString().isEmpty() || txtnombre.getText().toString().isEmpty() || txtemail.getText().toString().isEmpty() || txttelefono.getText().toString().isEmpty()) {
                  Toast.makeText(getApplicationContext(),"Complete los datos para continuar", Toast.LENGTH_SHORT).show();

              }else{
                 Reservar();
                 generarQR();
              }
          }
      });



    }


    public void obtenerDatosUsuario(FirebaseUser user) {
        progress.setMessage("Obteniendo datos");
        progress.show();
        mDatabase.child("Usuarios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    txtcedula.setText(snapshot.child(user.getUid()).child("cedula").exists() ? snapshot.child(user.getUid()).child("cedula").getValue().toString() : "" );
                    txtnombre.setText(snapshot.child(user.getUid()).child("nombres").exists() ? snapshot.child(user.getUid()).child("nombres").getValue().toString() : "");
                    txtemail.setText(snapshot.child(user.getUid()).child("correo").exists() ? snapshot.child(user.getUid()).child("correo").getValue().toString() : "");
                    txttelefono.setText(snapshot.child(user.getUid()).child("telefono").exists() ? snapshot.child(user.getUid()).child("telefono").getValue().toString() : "");
                    progress.dismiss();


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void Reservar(){
        progress.setMessage("Procesando");
        progress.show();

        //8888888888
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JSONObject postData = new JSONObject();
        try {
            postData.put("cedula", txtcedula.getText());
            postData.put("nombre", txtnombre.getText());
            postData.put("correo", txtemail.getText());
            postData.put("telefono", txttelefono.getText());
            postData.put("habitacion", nombreHabitacion);
            postData.put("fecha_entrada", fechaSeleccionada);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //8888888888

        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(
                Request.Method.POST,
                url+"reservations",
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),"Reservacion realizada", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(GenerarQR.this).add(jsonArrayRequest);

    }

    public void generarQR(){

        String datos = "Cedula: "+txtcedula.getText().toString()+"\nCliente: "+txtnombre.getText().toString()+"\nTelefono: "+txttelefono.getText().toString()+"\nCorreo: "+txtemail.getText().toString()+"\nHabitacion: " + nombre + "\nFecha reservacion: " + fechaSeleccionada;
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(datos, BarcodeFormat.QR_CODE,600,600);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            viewQr.setImageBitmap(bitmap);
            takeScreenshot();

        } catch (Exception e){

        }
    }
    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("hh:mm:ss", now);

        try {

            String mPath = Environment.getExternalStorageDirectory().toString() + "/Download/" + now + ".jpg";
            Log.d("la direccion", mPath);


            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            Toast.makeText(getApplicationContext(),"Codigo almacenado en descargas", Toast.LENGTH_SHORT).show();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }
}
