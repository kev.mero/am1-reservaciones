const mongoose = require("mongoose");
const { Schema } = mongoose;

// tabla transaccional: se guardaran registros de reservaciones tomando datos del usuario y de habitacion mediante el ID 
const ReservationSchema = new Schema(
  {
    usuario: { type: Schema.ObjectId, ref: 'User' },
    habitacion: { type: Schema.ObjectId, ref: 'Room' },
    fecha_entrada: { type: String },
    fecha_salida: { type: String },
    dias: { type: String }
    
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);

module.exports = mongoose.model("Reservation", ReservationSchema);
