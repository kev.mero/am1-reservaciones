const mongoose = require("mongoose");
const { Schema } = mongoose;
// tabla maestra: Guardara datos de usuario para las reservaciones
const UserSchema = new Schema(
  {
    cedula: { type: String },
    nombre: { type: String },
    correo: { type: String },
    telefono: { type: String },

  },
  { timestamps: { createdAt: true, updatedAt: true } }
);

module.exports = mongoose.model("User", UserSchema);
